# docker-environment

## 介绍
使用docker搭建常用开发环境

## 常用环境
1. docker安装mysql
```shell
docker run --restart=always -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD="123456" \
-v /Users/mayingfa/Development/docker/mysql/conf:/etc/mysql/conf.d \
-v /Users/mayingfa/Development/docker/mysql/logs:/var/log/mysql \
-v /Users/mayingfa/Development/docker/mysql/data:/var/lib/mysql \
-d mysql
```

2. docker安装redis
```shell
docker run --restart=always -p 6379:6379 --name redis \
-v /Users/mayingfa/Development/docker/redis/redis.conf:/etc/redis/redis.conf \
-v /Users/mayingfa/Development/docker/redis/data:/data \
-d redis redis-server /etc/redis/redis.conf --appendonly yes
```

